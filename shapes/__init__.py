# -*- coding: utf-8 -*-

import shapely
from shapely.geometry import Point
from DBTOOLS.DBTools import DBTools
import zipfile
import shapefile
import os.path
import sqlite3
import argparse

#a felhasználó által megadott elérési útvonal
zip_path = None

#az input argparse, a program címe
parser = argparse.ArgumentParser(description="GTFS to shape file")
#a változó csoport
required_args = parser.add_argument_group("Kötelező változók")
#az input helye
required_args.add_argument('-i', '--input',  dest="zip_path", help="A .zip fájl elérési útja", required = True)
#az output helye
required_args.add_argument('-o', '--output', dest="out_path", help="A shape fájl létrehozási helye és neve", required = True)

args = parser.parse_args()

args_dict = vars(args)
# Elérési útvonalak
zip_path = args_dict["zip_path"]
out_path = args_dict["out_path"]

#az eléréi út létezését definiáló rész
if os.path.exists(zip_path) == True:
    #a fájl létezését vizsgáló rész
    if os.path.isfile(zip_path) == True:
        #a fájl .zip létét vizsgáló rész
        filename, file_ext = os.path.splitext(zip_path)
        
        if file_ext == ".zip":
            #ha működik elkezdi beolvasni
            zipfile.ZipFile(zip_path)
        
        else:
            
            print("A megadott fájl nem .zip fájl.")
    
    else:
        
        print("A megadott elérési út nem fájlra mutat.")
        exit()
else:
    
    print("A megadott fájl nem létezik.")

#az adatbázis helye
path = "..\\adat\\db_adatbazis.db"
print(os.path.abspath(path))

db_tools = DBTools(path)

#elkezdi használni az előzetesen megvizsgált .zip fájlt
z = zipfile.ZipFile(zip_path)
#a stops.txt megnyitása
k = z.open("stops.txt")
#a shapes.txt megnyitása
l = z.open("shapes.txt")

#a stop.txt olvasása a .zip-en belül
sor_stop = k.readlines()

#sorokat számolja
i = 0
#a .txt adatbázisba írása
for line in sor_stop:
    #a címsor kihagyása
    if i != 0:
        #a sorok elválasztása a vessző karaktereknél
        y = line.decode().split(",")
        #különböző oszlopok helye a txt-ben
        stop_id = y [0]
        stop_name = y [1]
        stop_lat = y [2]
        stop_lon = y [3]
        
        #az fent definiált sorok adatbázisba írása
    i += 1
    
#ugyanaz mint fent, csak a shape.txt-re
sor_shape = l.readlines()

j = 0

for line in sor_shape:
    
    if j != 0:
        
        z = line.decode().split(",")
        
        shape_id = z [0]
        shape_pt_lan = z [1]
        shape_pt_lon = z [2]
        
        #print(shape_id)
        
    j += 1
    
#a stop adatbázisba írt sorok kijelölése írása
sorok_stop = db_tools.query_all_stop()

#shape író rész, pontként meghatározott shapetype
stop_shape = shapefile.Writer(shapeType=3)
#a shape-ben létrehozandó oszlop, ami tartalmazza a stop_id-t, és szöveges formátumú
stop_shape.field("stop_id", "C")

#for ciklus; sorokat számol
for l, sor in enumerate(sorok_stop):
    #a koordináták helyét jelöli
    pont = Point(sor[2], sor[3])
    #a koordináták shape-be írása                 
    stop_shape.point(sor[2], sor[3])
    #a stop_id-k shape-be írása
    stop_shape.record(sor[0], sor[1])
    
    #minden sorot végig olvas így hozza létre a shapet
    
#a shape kimentése, a felhasználó által meghatározott helyre
stop_shape.save(out_path)

print("A program lefutott, a shape fájl létrehozásra került a(z) " + out_path + " helyen.")