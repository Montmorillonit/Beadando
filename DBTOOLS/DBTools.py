# -*- coding: utf-8 -*-

import codecs
import sqlite3
import argparse
import sys, traceback

class DBTools:
    
    def __init__(self, path):
        
        try:
            
            self.__conn = sqlite3.connect(path)
            self.__cursor = self.__conn.cursor()
            
            #print("sikeres csatlakozás")
            
        except Exception:
            
            traceback.print_exc(file=sys.stdout)
            print("Nem sikerült csatlakozni az adatbázishoz")
            
        #oszlopok beszúrása
    def insert_rows_stop(self, stop_id, stop_name, stop_lat, stop_lon):
        
        query_insert = "INSERT INTO adat VALUES (?, ?, ?, ?)"
        
        self.__cursor.execute(query_insert, (stop_id, stop_name, stop_lat, stop_lon))
        
        self.__conn.commit()
        
        #oszlopok beszúrása
    def insert_rows_shape(self, shape_id, shape_pt_lan, stop_pt_lon):
        
        query_insert = "INSERT INTO adat_shape VALUES (?, ?, ?)"
        
        self.__cursor.execute(query_insert, (shape_id, shape_pt_lan, stop_pt_lon))
        
        self.__conn.commit()
        
        
    def query_all_stop(self):
        
        query = "SELECT * FROM db_adatbazis"
        
        self.__cursor.execute(query)
        result = self.__cursor.fetchall()
        
        return result
        
    def __del__(self):
        
        self.__conn.close()
        #print("kapcsolat sikeresen bezárult")