# -*- coding: utf-8 -*-
'''
Created on Dec 6, 2017
 
@author: hallgato
'''
 
import sqlite3
 
class DBTools:
   
    def __init__(self, path):
       
        try:
           
            self.__conn = sqlite3.connect(path)
            self.__cursor = self.__conn.cursor()
           
            print("sikeres csatlakozás")
           
        except Exception:
           
            print("Nem sikerült csatlakozni az adatbázishoz")
           
    def insert_rows(self, id, lon, lat):
       
        query_insert = "INSERT INTO points VALUES (?, ?, ?)"
       
        self.__cursor.execute(query_insert, (id, lon, lat))
       
        self.__conn.commit()
       
    def query_all(self):
       
        query = "SELECT * FROM points"
       
        self.__cursor.execute(query)
        result = self.__cursor.fetchall()
       
        return result
       
    def __del__(self):
       
        self.__conn.close()
        print("kapcsolat sikeresen bezárult")